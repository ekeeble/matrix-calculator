#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include <vector>

using namespace std;

template <class TY>
class matrix
{
public:
    matrix();
    ~matrix();
    void print();
    matrix<TY> transpose();
    TY determinant(matrix<TY> A);
    bool hasValidDim(matrix<TY> A);
    void readIn(ifstream &fin);
    void readIn2(ifstream &dataIn, matrix<TY> &m1);
    matrix<TY> operator*(matrix<TY> A);
    void operator=(matrix<TY> A);
    matrix<TY> operator+(matrix<TY> A);
    bool operator==(matrix<TY> A);
    matrix<TY> operator-(matrix<TY> A);
    matrix<TY> operator^(matrix<TY> A);
private:
    int numRows;
    int numCols; 
    vector<TY> theMatrix;
public:
    matrix(vector<TY> a, int rows, int cols);
};



