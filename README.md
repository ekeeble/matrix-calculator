Matrix Calculator

by: Erica Keeble

Still a work in progress. Currently working on converting to template class.

Development progess tracked on private gitlab account. Transferred to this 
public account for viewing. 

Out-of-class Independent Project. Researched, derived and implemented own algorithms using recursion and 
object-oriented programming to calculate matrix multiplication, determinants, transpose, addition, and subtraction. 