#include <iostream>
//#include "matrix.h"

using namespace std;

//class declaration
template <class TY>
class matrix
{
public:
    matrix();
    ~matrix();
    // turns an existing vector into a matrix
    matrix<TY> cpymatrix(vector<TY> a, int rows, int cols);
    void print();
    matrix<TY> transpose();
    TY determinant(matrix<TY> A);
    // checks if the two matricies can be multiplied based on dimensions
    bool hasValidDim(matrix<TY> A);
    void readIn(ifstream &fin);
    void readIn2(ifstream &dataIn, matrix<TY> &m1);
    matrix<TY> operator*(matrix<TY> A);
    void operator=(matrix<TY> A);
    matrix<TY> operator+(matrix<TY> A);
    bool operator==(matrix<TY> A);
    matrix<TY> operator-(matrix<TY> A);
    matrix<TY> operator^(matrix<TY> A);
private:
    int numRows;
    int numCols;
    vector<TY> theMatrix;
    //ifstream fin; 
};

int main()
{
    matrix<TY> m1;
    ifstream fin, dataIn;
    fin.open("matrices.txt");
    if (!fin.is_open())
    {
        cout << "File did not open." << endl;
        return 0;
    }
    dataIn.open("iris dataset - setosa.txt");
    if (!dataIn.is_open())
    {
        cout << "File did not open." << endl;
        return 0;
    }
    
    m1.readIn2(dataIn, m1);

    /*
    matrix m1, m2, m3;
    m1.readIn(fin);
    m2.readIn(fin);
    m1.print();
    m2.print();

    m3 = m1 ^ m2;

    m3.print();

    fin.close();
    */

    return 0;
}

//freecodecamp
//top in demand frameworks
//leetcode