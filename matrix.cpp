/**************************************************************************//**
* @file
* @brief Completes these matrix calculations: multiplication (2 methods), 
*        determinant, transpose, addition, and subtraction.      
*
* @mainpage Matrix Calculator
*
* @section course_section Author Information
*
* @author Erica Keeble
*
* @date August 1, 2018
*
* @section program_section Program Information
*
* @details Functions in the Matrix class execute matrix addition, subtraction, 
*       determinants, and two algorithms for matrix multiplication. More details
*       regarding the algorithms for matrix multiplication are found in the 
*       attached PDF. 
*
* @par Usage
* @verbatim
* c:\> matrix.exe 
* @endverbatim
*
* @section todos_bugs_modification_section ToDo, Bugs, and Modifications
*       @ToDo In the process of converting to a template class. Will restore to
*           previous working version.
*
* @par Modification and Development Timeline:
*   <a href="https://gitlab.mcs.sdsmt.edu/7431673/csc215s18programs
/commits/master">Click here for commit log.</a>
*
********************************************************************************/

#include "matrix.h"

/**************************************************************************//**
* @author Erica Keeble
* @par Description:
* Constructor
******************************************************************************/
template <class TY>
matrix<TY>::matrix()
{
    numRows = 0;
    numCols = 0;
}

/**************************************************************************//**
* @author Erica Keeble
* @par Description:
* Copy constructor - turns an existing vector into a matrix
*
* @param[in]       a - existing matrix data
* @param[in]       rows - number of rows in matrix
* @param[in]       cols - number of columns in matrix
******************************************************************************/
template<class TY>
matrix<TY>::matrix(vector<TY> a, int rows, int cols)
{
    matrix A;
    A.numRows = rows;
    A.numCols = cols;
    A.theMatrix = a;

    return A;
}

/**************************************************************************//**
* @author Erica Keeble
* @par Description:
* Destructor
******************************************************************************/
template <class TY>
matrix<TY>::~matrix()
{
    theMatrix.clear();
}

/**************************************************************************//**
* @author Erica Keeble
* @par Description:
* Prints out the matrix to the screen
******************************************************************************/
template <class TY>
void matrix<TY>::print()
{
    matrix A;
    A = transpose();
    for (int i = 0; i < numRows * numCols; i++)
    {
        if(i % numCols == 0)
            cout << endl;

        cout << setw(6) << A.theMatrix[i];
    }
    cout << endl << endl;
}

/**************************************************************************//**
* @author Erica Keeble
* @par Description:
* Reads in matrices from an input file
*
* @param[in]       fin - input file stream
******************************************************************************/
template <class TY>
void matrix<TY>::readIn(ifstream &fin)
{
    //temporary variables for reading in matrix
    char a[20];
    TY b;

    //read in number of rows and cols
    fin >> numRows >> numCols;

    //read in matrix from file into char a
    while (fin >> a && strcmp(a, ")"))
    {
        //A.push_back(a);
        if(strcmp(a, ")") && strcmp(a, "(") && strcmp(a, ":"))
        {
            b = atoi(a);
            theMatrix.push_back(b);
        }
    }
}

/**************************************************************************//**
* @author Erica Keeble
* @par Description:
* Overloaded operator for matrix multiplication using method 1
*
* @param[in]       A - matrix to be multiplied
*
* @returns if successful, multiplied matrix
* @returns if failed, matrix with contents -1
******************************************************************************/
template <class TY>
matrix<TY> matrix<TY>::operator*(matrix<TY> A)
{
    //the lhs matrix is "Matrix", the rhs matrix is "A.matrix"
    matrix mult, failed;
    mult.numRows = mult.numCols = failed.numRows = failed.numCols = 1;
    failed.theMatrix.push_back(-1); //returns this matrix if calculation fails
    int i = 0;
    int j = 0;
    bool endCol_j = false;
    bool endCol_i = false;
    int matrixindex_row = 0;
    int matrixindex_col = 0;
    int startPt_j = 0;
    TY **runningTotals;
    runningTotals = new (nothrow) TY *[numCols];
    if (runningTotals == nullptr)   //allocate memory
    {
        cout << "Memory allocation failed" << endl;
        return failed;
    }
    for (int i = 0; i < (numRows * A.numRows); i++)
    {
        runningTotals[i] = new (nothrow) TY;
        if (runningTotals == nullptr)
        {
            cout << "Memory allocation failed" << endl;
            return failed;
        }
    }

    //make sure matrices are correct size to be multiplied
    if (!hasValidDim(A))
        return failed;
       
    //take the transpose of the matrix A
    A = A.transpose();

    while(i < theMatrix.size()) //while not at the end of theMatrix 
    {
        if (endCol_i)  //if i is at end of col
        {
            startPt_j = j;  //update start pt of j
            matrixindex_row++;  //update row of runningTotal matrix
            matrixindex_col = 0;
            endCol_i = false;
        }
        if (i % numRows == numRows - 1)
            endCol_i = true;

        j = startPt_j;
        while(!endCol_j) //increment j until end of col
        {
            runningTotals[matrixindex_row][matrixindex_col] =
                theMatrix[i] * A.theMatrix[j];
            matrixindex_col++;
            if (j % (A.numRows) == A.numRows - 1)
                endCol_j = true;        
            j++;
        } 
        endCol_j = false;   //reset endCol_j and increment i
        i++;
    }

    //insert running totals into the final matrix mult.theMatrix
    TY sum = 0;
    for (int j = 0; j < matrixindex_col; j++)
    {
        for (int i = 0; i <= matrixindex_row; i++)
        {
            sum += runningTotals[i][j];
        }
        mult.theMatrix.push_back(sum);
        sum = 0;
    }
    mult.numRows = numRows;
    mult.numCols = A.numRows; //becuase A has been transposed, use rows instead of cols
    mult = mult.transpose();
    return mult;
}

template <class TY>
void matrix<TY>::operator=(matrix<TY> A)
{
    theMatrix = A.theMatrix;   
    numCols = A.numCols;
    numRows = A.numRows;
}

/**************************************************************************//**
* @author Erica Keeble
* @par Description:
* Overloaded operator for addition of two matrices
*
* @param[in]       A - rhs matrix to be added
*
* @returns added matrix if sucessful
* @returns matrix with contents -1 if failed
******************************************************************************/
template <class TY>
matrix<TY> matrix<TY>::operator+(matrix<TY> A)
{
    matrix added;
    TY num;
    //if not the corrects sizes to be added, return a matrix containing only '-1'
    if (numRows != A.numRows || numCols != A.numCols)
    {
        added.theMatrix.push_back(-1);
        return added;
    }

    //add each element of the two matrices
    for (int i = 0; i < numRows * numCols; i++)
    {
        num = theMatrix[i] + A.theMatrix[i];
        added.theMatrix.push_back(num);
    }

    added.numRows = numRows;
    added.numCols = numCols;
    return added;
}

/**************************************************************************//**
* @author Erica Keeble
* @par Description:
* Overloaded boolean equals operator 
*
* @param[in]       A - rhs matrix to be compared
*
* @returns true if the two matricies are the same
* @returns false otherwise
******************************************************************************/
template <class TY>
bool matrix<TY>::operator==(matrix<TY> A)
{
    if (theMatrix == A.theMatrix && numRows == A.numRows)
        return true;
    else
        return false;
}

/**************************************************************************//**
* @author Erica Keeble
* @par Description:
* Overloaded operator for subtraction of matrices
*
* @param[in]       A - rhs matrix to be subtracted
*
* @returns subtracted matrix if successful
* @returns matrix with contents -1 if failed
******************************************************************************/
template <class TY>
matrix<TY> matrix<TY>::operator-(matrix<TY> A)
{
    matrix subtracted;
    TY num;
    if (numRows != A.numRows || numCols != A.numCols)
    {
        subtracted.theMatrix.push_back(-1);
        return subtracted;
    }

    //preform subtraction
    for (int i = 0; i < numRows * numCols; i++)
    {
        num = theMatrix[i] - A.theMatrix[i];
        subtracted.theMatrix.push_back(num);
    }

    subtracted.numRows = numRows;
    subtracted.numCols = numCols;
    return subtracted;
}

/**************************************************************************//**
* @author Erica Keeble
* @par Description:
* Finds the transpose of the matrix
*
* @returns transpose matrix
******************************************************************************/
template <class TY>
matrix<TY> matrix<TY>::transpose()
{
    matrix A;
    TY val;
    for (int i = 0; i < numRows; i++)
    {
        for (int j = 0; j < numCols; j++)
        {
            val = theMatrix[i + (j * numRows)];
            A.theMatrix.push_back(val);
        }
    }

    A.numCols = numRows;
    A.numRows = numCols;
    return A;
}

// checks if the two matrices can be multiplied based on dimensions
/**************************************************************************//**
* @author Erica Keeble
* @par Description:
* Verifies that the dimensions of two matrices are compatible for multiplication
*
* @param[in]       A - matrix to be compared
*
* @returns true if they can be multiplied
* @returns false otherwise
******************************************************************************/
template <class TY>
bool matrix<TY>::hasValidDim(matrix<TY> A)
{
    if (numCols != A.numRows)
        return false;

    if (numRows != A.numCols)
    {
        if (numRows != 1 && A.numCols != 1)
            return false;
    }

    if (numRows <= 0 || numCols <= 0 || A.numRows <= 0 || A.numCols <= 0)
        return false;

    return true;
}

/**************************************************************************//**
* @author Erica Keeble
* @par Description:
* Takes in a matrix and returns its determinant
*
* @param[in]       A - matrix for which to calculate determinant
*
* @returns Determinant if successful
* @returns -1 otherwise
******************************************************************************/
template <class TY>
TY matrix<TY>::determinant(matrix<TY> A)
{
    //create numCols matrices. Vector that has a bunch more vectors in it
    vector<vector<TY>> submatrices; //this contains the new matrices formed
    TY* multipliers;   //this contains multipliers for those new matrices
    multipliers = new (nothrow) TY[A.numRows];
    if (multipliers == nullptr)
    {
        cout << "Memory allocation failed" << endl;
        return -1;
    }
    int whatColWeOn = -1;
    TY det = 0;

    //base case. If a 2x2 matrix, we can find det and return that value
    if (A.numRows == 2)
    {
        det = (A.theMatrix[0] * A.theMatrix[3]) - 
            (A.theMatrix[1] * A.theMatrix[2]);
        return det;
    }

    //create all the submatrices and record their multipliers
    vector<TY> v;
    for (int i = 0; i < A.numRows; i++)   //create the empty submatrices
        submatrices.push_back(v);

    for(int i = 0; i < A.numRows * A.numRows; i++)   //walk through every element
    {
        if (i % A.numRows == 0)   //if we at the start of a new col now
        {
            whatColWeOn++;
            multipliers[whatColWeOn] = A.theMatrix[i];    //add that num to multipliers
            if (whatColWeOn % 2 == 1)   //make the multipliter negative if necessary
            {
                multipliers[whatColWeOn] = multipliers[whatColWeOn] * (-1);
            }
            i++;    //increment i because we aint putting the first row values in anything else
        }

        //add all the next numbers in the col to all the other submatrices
        for(int j = 0; j < A.numRows; j++)
        {
            if(j != whatColWeOn)
                submatrices[j].push_back(A.theMatrix[i]);
        }        
    }

    //factor in multiplier to value, and add to final determinant
    for (int i = 0; i < A.numRows; i++)
    {
        det += multipliers[i] * determinant(cpymatrix(submatrices[i], 
            A.numRows - 1, A.numCols - 1));
    }
    return det;
}

//matrix multiplication method 2
/**************************************************************************//**
* @author Erica Keeble
* @par Description:
* Overloaded operator for matrix multiplication using method 2
*
* @param[in]       A - matrix to be multiplied
*
* @returns multiplied matrix if successful
* @returns failed matrix with contents -1 if failed
******************************************************************************/
template <class TY>
matrix<TY> matrix<TY>::operator^(matrix<TY> A)
{
    matrix m1, m2, m3;
    m2 = A;
    //take the transpose of the first matrix
    m1 = transpose();
    int j_start = 0;
    int j = 0;
    vector<TY> resultMatrix;
    TY element = 0;

    int numElements = m1.numCols * m1.numRows;

    for (int k = 0; k < m2.numCols; k++)
    {
        //reset j to the next start point
        if (k > 0)
            j_start = j + m1.numRows;

        j = j_start;

        for (int i = 0; i < numElements; i++)
        {
            //do the multiplications for the element and add to element total
            element += m1.theMatrix[i] * m2.theMatrix[j];

            //if we are at the end of the row
            if (j == j_start + m2.numRows - 1)
            {
                //add element to result matrix
                resultMatrix.push_back(element);

                element = 0;

                //reset j to j_start
                j = j_start;
            }
            else
                j++;
        }
    }

    //convert to a matrix and return that matrix
    m3 = cpymatrix(resultMatrix, m1.numCols, m2.numCols);

    return m3;
}

/**************************************************************************//**
* @author Erica Keeble
* @par Description:
* Specifically reads in the iris dataset for the setosa and virginica data
* for testing purposes.
* 
* @param[in]       dataIn - input file stream
* @param[in]       m1 - matrix to read into
******************************************************************************/
template <class TY>
void matrix<TY>::readIn2(ifstream &dataIn, matrix<TY> &m1)
{
    char value[7];
    TY temp;  
    m1.numCols = 4;

    while (!dataIn.eof())
    {
        //read data in as a string        
        dataIn >> value;

        //if the value read in is garbage, skip it.
        if (strcmp(value, "setosa") == 0 || strcmp(value, "virginica") == 0)
            m1.numRows++;
        //otherwise, convert it to a number and store it
        else
        {
            temp = stoi(value);
            m1.theMatrix.push_back(temp);
        }
    }
    m1 = m1.transpose();

    return;
}